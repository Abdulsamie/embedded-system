/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.c
  * @brief          : Main program body
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2022 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under BSD 3-Clause license,
  * the "License"; You may not use this file except in compliance with the
  * License. You may obtain a copy of the License at:
  *                        opensource.org/licenses/BSD-3-Clause
  *
  ******************************************************************************
  */
/* USER CODE END Header */
/* Includes ------------------------------------------------------------------*/
#include "main.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include <stdio.h>
#include <string.h>
/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */

/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */
//I2C ADDRESS
#define MPU6050_DEFAULT_DEVICE_ADDRESS (0x68<<1)

//MPU6050 CONFIG REGISTER ADDRESSES
#define MPU6050_RA_WHOAMI 0x75
#define MPU6050_RA_PWR_MGMT_1 0x6B
#define MPU6050_RA_CONFIG 0x1A
#define MPU6050_RA_SMPLRT_DIV 0x19
#define MPU6050_RA_GYRO_CONFIG 0x1B
#define MPU6050_RA_ACCEL_CONFIG 0x1C
#define MPU6050_RA_INT_PIN_CFG 0x37
#define MPU6050_RA_SIGNAL_PATH_RESET 0x68
#define MPU6050_RA_INT_ENABLE 0x38

//MPU6050 DATA REGISTERS
#define MPU6050_RA_ACCEL_XOUT_H 0x3B
#define MPU6050_RA_ACCEL_XOUT_L 0x3C
#define MPU6050_RA_ACCEL_YOUT_H 0x3D
#define MPU6050_RA_ACCEL_YOUT_L 0x3E
#define MPU6050_RA_ACCEL_ZOUT_H 0x3F
#define MPU6050_RA_ACCEL_ZOUT_L 0x40
#define MPU6050_RA_TEMP_OUT_H   0x41
#define MPU6050_RA_TEMP_OUT_L   0x42
#define MPU6050_RA_GYRO_XOUT_H  0x43
#define MPU6050_RA_GYRO_XOUT_L  0x44
#define MPU6050_RA_GYRO_YOUT_H  0x45
#define MPU6050_RA_GYRO_YOUT_L  0x46
#define MPU6050_RA_GYRO_ZOUT_H  0x47
#define MPU6050_RA_GYRO_ZOUT_L  0x48

//SPECIAL REGISTER VALUES
#define MPU6050_CLOCK_PLL_ZGYRO 0x3
#define MPU6050_GYRO_FS_2000 0x3<<3
#define MPU6050_ACCEL_FS_16 0x3<<3
/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/
I2C_HandleTypeDef hi2c1;
DMA_HandleTypeDef hdma_i2c1_rx;

UART_HandleTypeDef huart2;

/* USER CODE BEGIN PV */
static uint32_t MPU6050_started;
uint16_t mpu6050_raw_data[7];
struct {
  float accelX;
  float accelY;
  float accelZ;
  float temp;
  float gyroX;
  float gyroY;
  float gyroZ;
} mpu6050_data;

/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
static void MX_GPIO_Init(void);
static void MX_DMA_Init(void);
static void MX_USART2_UART_Init(void);
static void MX_I2C1_Init(void);
/* USER CODE BEGIN PFP */

uint8_t MPU6050_Read(uint8_t devAddr, uint8_t regAddr);
void MPU6050_Write(uint8_t devAddr, uint8_t regAddr, uint8_t data);
void MPU6050_Init();
long adc_convert_int(long val, long in_min, long in_max, long out_min, long out_max);
float adc_convert_float(long val, long in_min, long in_max, float out_min, float out_max);
void MPU6050_get_acceleration(int16_t* x, int16_t* y, int16_t* z);
void MPU6050_print_acceleration(void);
void MPU6050_get_rotation(int16_t* x, int16_t* y, int16_t* z);
void MPU6050_print_rotation(void);
void HAL_GPIO_EXTI_Callback(uint16_t GPIO_Pin);
void HAL_I2C_MemRxCpltCallback(I2C_HandleTypeDef *hi2c);
/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */

/* USER CODE END 0 */

/**
  * @brief  The application entry point.
  * @retval int
  */
int main(void)
{
  /* USER CODE BEGIN 1 */

  /* USER CODE END 1 */

  /* MCU Configuration--------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();

  /* USER CODE BEGIN Init */

  /* USER CODE END Init */

  /* Configure the system clock */
  SystemClock_Config();

  /* USER CODE BEGIN SysInit */

  /* USER CODE END SysInit */

  /* Initialize all configured peripherals */
  MX_GPIO_Init();
  MX_DMA_Init();
  MX_USART2_UART_Init();
  MX_I2C1_Init();
  /* USER CODE BEGIN 2 */

  // Init MPU6050
  MPU6050_Init();

  // Read WHOAMI Register of MPU6050
//  uint8_t answer = MPU6050_Read(MPU6050_DEFAULT_DEVICE_ADDRESS, MPU6050_RA_WHOAMI);

/*  HAL_I2C_Mem_Read(&hi2c1,
                   MPU6050_DEFAULT_DEVICE_ADDRESS,
                   MPU6050_RA_ACCEL_XOUT_H,
                   I2C_MEMADD_SIZE_8BIT,
                   (uint8_t*)&mpu6050_raw_data,
                   sizeof(mpu6050_raw_data),
                   HAL_MAX_DELAY);*/

  /* USER CODE END 2 */

  /* Infinite loop */
  /* USER CODE BEGIN WHILE */
  while (1)
  {
//    MPU6050_print_acceleration();
//    MPU6050_print_rotation();

//    HAL_Delay(500);

    __SEV();
    /* USER CODE END WHILE */

    /* USER CODE BEGIN 3 */
  }
  /* USER CODE END 3 */
}

/**
  * @brief System Clock Configuration
  * @retval None
  */
void SystemClock_Config(void)
{
  RCC_OscInitTypeDef RCC_OscInitStruct = {0};
  RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};
  RCC_PeriphCLKInitTypeDef PeriphClkInit = {0};

  /** Initializes the RCC Oscillators according to the specified parameters
  * in the RCC_OscInitTypeDef structure.
  */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSI;
  RCC_OscInitStruct.HSIState = RCC_HSI_ON;
  RCC_OscInitStruct.HSICalibrationValue = RCC_HSICALIBRATION_DEFAULT;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_NONE;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    Error_Handler();
  }
  /** Initializes the CPU, AHB and APB buses clocks
  */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_HSI;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV1;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_0) != HAL_OK)
  {
    Error_Handler();
  }
  PeriphClkInit.PeriphClockSelection = RCC_PERIPHCLK_I2C1;
  PeriphClkInit.I2c1ClockSelection = RCC_I2C1CLKSOURCE_HSI;
  if (HAL_RCCEx_PeriphCLKConfig(&PeriphClkInit) != HAL_OK)
  {
    Error_Handler();
  }
}

/**
  * @brief I2C1 Initialization Function
  * @param None
  * @retval None
  */
static void MX_I2C1_Init(void)
{

  /* USER CODE BEGIN I2C1_Init 0 */

  /* USER CODE END I2C1_Init 0 */

  /* USER CODE BEGIN I2C1_Init 1 */

  /* USER CODE END I2C1_Init 1 */
  hi2c1.Instance = I2C1;
  hi2c1.Init.Timing = 0x2000090E;
  hi2c1.Init.OwnAddress1 = 0;
  hi2c1.Init.AddressingMode = I2C_ADDRESSINGMODE_7BIT;
  hi2c1.Init.DualAddressMode = I2C_DUALADDRESS_DISABLE;
  hi2c1.Init.OwnAddress2 = 0;
  hi2c1.Init.OwnAddress2Masks = I2C_OA2_NOMASK;
  hi2c1.Init.GeneralCallMode = I2C_GENERALCALL_DISABLE;
  hi2c1.Init.NoStretchMode = I2C_NOSTRETCH_DISABLE;
  if (HAL_I2C_Init(&hi2c1) != HAL_OK)
  {
    Error_Handler();
  }
  /** Configure Analogue filter
  */
  if (HAL_I2CEx_ConfigAnalogFilter(&hi2c1, I2C_ANALOGFILTER_ENABLE) != HAL_OK)
  {
    Error_Handler();
  }
  /** Configure Digital filter
  */
  if (HAL_I2CEx_ConfigDigitalFilter(&hi2c1, 0) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN I2C1_Init 2 */

  /* USER CODE END I2C1_Init 2 */

}

/**
  * @brief USART2 Initialization Function
  * @param None
  * @retval None
  */
static void MX_USART2_UART_Init(void)
{

  /* USER CODE BEGIN USART2_Init 0 */

  /* USER CODE END USART2_Init 0 */

  /* USER CODE BEGIN USART2_Init 1 */

  /* USER CODE END USART2_Init 1 */
  huart2.Instance = USART2;
  huart2.Init.BaudRate = 115200;
  huart2.Init.WordLength = UART_WORDLENGTH_8B;
  huart2.Init.StopBits = UART_STOPBITS_1;
  huart2.Init.Parity = UART_PARITY_NONE;
  huart2.Init.Mode = UART_MODE_TX_RX;
  huart2.Init.HwFlowCtl = UART_HWCONTROL_NONE;
  huart2.Init.OverSampling = UART_OVERSAMPLING_16;
  huart2.Init.OneBitSampling = UART_ONE_BIT_SAMPLE_DISABLE;
  huart2.AdvancedInit.AdvFeatureInit = UART_ADVFEATURE_NO_INIT;
  if (HAL_UART_Init(&huart2) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN USART2_Init 2 */

  /* USER CODE END USART2_Init 2 */

}

/**
  * Enable DMA controller clock
  */
static void MX_DMA_Init(void)
{

  /* DMA controller clock enable */
  __HAL_RCC_DMA1_CLK_ENABLE();

  /* DMA interrupt init */
  /* DMA1_Channel2_3_IRQn interrupt configuration */
  HAL_NVIC_SetPriority(DMA1_Channel2_3_IRQn, 1, 0);
  HAL_NVIC_EnableIRQ(DMA1_Channel2_3_IRQn);

}

/**
  * @brief GPIO Initialization Function
  * @param None
  * @retval None
  */
static void MX_GPIO_Init(void)
{
  GPIO_InitTypeDef GPIO_InitStruct = {0};

  /* GPIO Ports Clock Enable */
  __HAL_RCC_GPIOF_CLK_ENABLE();
  __HAL_RCC_GPIOA_CLK_ENABLE();
  __HAL_RCC_GPIOB_CLK_ENABLE();

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(LD3_GPIO_Port, LD3_Pin, GPIO_PIN_RESET);

  /*Configure GPIO pin : PB1 */
  GPIO_InitStruct.Pin = GPIO_PIN_1;
  GPIO_InitStruct.Mode = GPIO_MODE_IT_RISING;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);

  /*Configure GPIO pin : LD3_Pin */
  GPIO_InitStruct.Pin = LD3_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(LD3_GPIO_Port, &GPIO_InitStruct);

  /*Configure GPIO pin : PB4 */
  GPIO_InitStruct.Pin = GPIO_PIN_4;
  GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  GPIO_InitStruct.Alternate = GPIO_AF2_EVENTOUT;
  HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);

  /* EXTI interrupt init*/
  HAL_NVIC_SetPriority(EXTI0_1_IRQn, 1, 0);
  HAL_NVIC_EnableIRQ(EXTI0_1_IRQn);

}

/* USER CODE BEGIN 4 */
uint8_t MPU6050_Read(uint8_t devAddr, uint8_t regAddr)
{
  uint8_t data;
  HAL_StatusTypeDef status = HAL_I2C_Mem_Read(&hi2c1, devAddr, regAddr,
                                              I2C_MEMADD_SIZE_8BIT, &data, 1, 100);
  if (status != HAL_OK)
  {
    Error_Handler();  // Does not return
    return 0;
  }
  return data;
}

void MPU6050_Write(uint8_t devAddr, uint8_t regAddr, uint8_t data)
{
  HAL_StatusTypeDef status = HAL_I2C_Mem_Write(&hi2c1, devAddr, regAddr,
                                              I2C_MEMADD_SIZE_8BIT, &data, 1, 100);
  if (status != HAL_OK)
  {
    Error_Handler();
  }
}

void MPU6050_Init()
{
  //reset the whole module first
  MPU6050_Write(MPU6050_DEFAULT_DEVICE_ADDRESS, MPU6050_RA_PWR_MGMT_1, 1<<7);
  //wait for 50ms for the gyro to stable
  HAL_Delay(50);
  MPU6050_Write(MPU6050_DEFAULT_DEVICE_ADDRESS, MPU6050_RA_PWR_MGMT_1, MPU6050_CLOCK_PLL_ZGYRO);
  //PLL with Z axis gyroscope reference
  MPU6050_Write(MPU6050_DEFAULT_DEVICE_ADDRESS, MPU6050_RA_CONFIG, 0x01);
  //DLPF_CFG = 1: Fs=1khz; bandwidth=42hz
  MPU6050_Write(MPU6050_DEFAULT_DEVICE_ADDRESS, MPU6050_RA_SMPLRT_DIV, 1);
  // 1kHz/2
  MPU6050_Write(MPU6050_DEFAULT_DEVICE_ADDRESS, MPU6050_RA_GYRO_CONFIG, MPU6050_GYRO_FS_2000);
  //Gyro full scale setting to ± 2000°/s
  MPU6050_Write(MPU6050_DEFAULT_DEVICE_ADDRESS, MPU6050_RA_ACCEL_CONFIG, MPU6050_ACCEL_FS_16);
  //Accel full scale setting to ± 16g
  MPU6050_Write(MPU6050_DEFAULT_DEVICE_ADDRESS, MPU6050_RA_INT_PIN_CFG, 1<<4);
  //interrupt status bits are cleared on any read operation
  MPU6050_Write(MPU6050_DEFAULT_DEVICE_ADDRESS, MPU6050_RA_SIGNAL_PATH_RESET, 0x07);
  //reset gyro and accel sensor
  MPU6050_Write(MPU6050_DEFAULT_DEVICE_ADDRESS, MPU6050_RA_INT_ENABLE, 1<<0);
  //interrupt occurs when data is ready.
  MPU6050_started = 1;
}

long adc_convert_int(long val, long in_min, long in_max, long out_min, long out_max)
{
  return (val - in_min) * (out_max - out_min) / (in_max - in_min) + out_min;
}

float adc_convert_float(long val, long in_min, long in_max, float out_min, float out_max)
{
  return ((float) val - (float) in_min) * (out_max - out_min) / ((float) in_max - (float) in_min) + out_min;
}

void MPU6050_get_acceleration(int16_t* x, int16_t* y, int16_t* z) {
  uint8_t buffer[6];
  HAL_I2C_Mem_Read(&hi2c1,
                   MPU6050_DEFAULT_DEVICE_ADDRESS,
                   MPU6050_RA_ACCEL_ZOUT_H,
                   I2C_MEMADD_SIZE_8BIT,
                   &buffer,
                   sizeof(buffer),
                   HAL_MAX_DELAY);
  *x = (((int16_t)buffer[0]) << 8) | buffer[1];
  *y = (((int16_t)buffer[2]) << 8) | buffer[3];
  *z = (((int16_t)buffer[4]) << 8) | buffer[5];
}

void MPU6050_print_acceleration(void)
{
  int16_t x, y, z;
  MPU6050_get_acceleration(&x, &y, &z);
  x = __REVSH(x);
  y = __REVSH(y);
  z = __REVSH(z);

  long lx, ly, lz;

  lx = adc_convert_int(x, INT16_MIN, INT16_MAX, -16, 16);
  ly = adc_convert_int(y, INT16_MIN, INT16_MAX, -16, 16);
  lz = adc_convert_int(z, INT16_MIN, INT16_MAX, -16, 16);

  uint8_t buffer[64];

  snprintf(buffer, sizeof(buffer), "x: %dg    y: %dg    z: %dg\r\n", lx, ly, lz);
  HAL_UART_Transmit(&huart2, buffer, strlen(buffer), HAL_MAX_DELAY);
}

void MPU6050_get_rotation(int16_t* x, int16_t* y, int16_t* z)
{
  uint8_t buffer[6];
  HAL_I2C_Mem_Read(&hi2c1,
                   MPU6050_DEFAULT_DEVICE_ADDRESS,
                   MPU6050_RA_GYRO_XOUT_H,
                   I2C_MEMADD_SIZE_8BIT,
                   &buffer,
                   sizeof(buffer),
                   HAL_MAX_DELAY);
  *x = (((int16_t)buffer[0]) << 8) | buffer[1];
  *y = (((int16_t)buffer[2]) << 8) | buffer[3];
  *z = (((int16_t)buffer[4]) << 8) | buffer[5];
}

void MPU6050_print_rotation(void)
{
  int16_t x, y, z;
  MPU6050_get_acceleration(&x, &y, &z);
  x = __REVSH(x);
  y = __REVSH(y);
  z = __REVSH(z);

  long lx, ly, lz;

  lx = adc_convert_int(x, INT16_MIN, INT16_MAX, -2000, 2000);
  ly = adc_convert_int(y, INT16_MIN, INT16_MAX, -2000, 2000);
  lz = adc_convert_int(z, INT16_MIN, INT16_MAX, -2000, 2000);

  uint8_t buffer[64];

  snprintf(buffer, sizeof(buffer), "x: %d°/s    y: %d°/s    z: %d°/s\r\n", lx, ly, lz);
  HAL_UART_Transmit(&huart2, buffer, strlen(buffer), HAL_MAX_DELAY);
}

void HAL_GPIO_EXTI_Callback(uint16_t GPIO_Pin)
{
  if(GPIO_Pin != GPIO_PIN_1) {
    return;
  }
  if(!MPU6050_started) {
    return;
  }
//  HAL_GPIO_TogglePin(LD3_GPIO_Port, LD3_Pin);
  HAL_GPIO_WritePin(LD3_GPIO_Port, LD3_Pin, GPIO_PIN_SET);
  HAL_I2C_Mem_Read_DMA(&hi2c1,
                       MPU6050_DEFAULT_DEVICE_ADDRESS,
                       MPU6050_RA_ACCEL_XOUT_H,
                       I2C_MEMADD_SIZE_8BIT,
                       (uint8_t*)&mpu6050_raw_data,
                       sizeof(mpu6050_raw_data));
}

void HAL_I2C_MemRxCpltCallback(I2C_HandleTypeDef *hi2c)
{
  HAL_GPIO_WritePin(LD3_GPIO_Port, LD3_Pin, GPIO_PIN_RESET);
  mpu6050_data.accelX = adc_convert_float(__REVSH(mpu6050_raw_data[0]), INT16_MIN, INT16_MAX, -16.0, 16.0);
  mpu6050_data.accelY = adc_convert_float(__REVSH(mpu6050_raw_data[1]), INT16_MIN, INT16_MAX, -16.0, 16.0);
  mpu6050_data.accelZ = adc_convert_float(__REVSH(mpu6050_raw_data[2]), INT16_MIN, INT16_MAX, -16.0, 16.0);
  mpu6050_data.temp = (float) __REVSH(mpu6050_raw_data[3]) / 340 + 36.53;
  mpu6050_data.gyroX = adc_convert_float(__REVSH(mpu6050_raw_data[4]), INT16_MIN, INT16_MAX, -2000.0, 2000.0);
  mpu6050_data.gyroY = adc_convert_float(__REVSH(mpu6050_raw_data[5]), INT16_MIN, INT16_MAX, -2000.0, 2000.0);
  mpu6050_data.gyroZ = adc_convert_float(__REVSH(mpu6050_raw_data[6]), INT16_MIN, INT16_MAX, -2000.0, 2000.0);
}


/* USER CODE END 4 */

/**
  * @brief  This function is executed in case of error occurrence.
  * @retval None
  */
void Error_Handler(void)
{
  /* USER CODE BEGIN Error_Handler_Debug */
  /* User can add his own implementation to report the HAL error return state */
  __disable_irq();

  /* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t *file, uint32_t line)
{
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
     ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
